--add 5 artist 
INSERT INTO artists 
(name)
VALUES
("Taylor Swift"), --3
("Lady Gaga"), --4
("Justine Bieber"), --5
("Aryana Grande"), --6
("Bruno Mars"); --7

-- 2 albums for taylor [3]
INSERT INTO albums
(
    album_title,
    date_released,
    artist_id
)
VALUES
(
    "Fearless",
    "2008-1-1",
    3
),
(
    "Red",
    "2012-1-1",
    3
);
--fearless album
INSERT INTO songs
(
    song_name,
    length,
    genre,
    album_id
)
VALUES
(
    "Fearless",
    246,
    "Pop Rock",
    6
),
(
    "State of Grace",
    213,
    "Rock",
    6
);
--red album
INSERT INTO songs
(
    song_name,
    length,
    genre,
    album_id
)
VALUES
(
    "Love Story",
    312,
    "Country Pop",
    7
),
(
    "Begin Again",
    304,
    "Country",
    7
);

-- 2 albums for lady gaga [4]
INSERT INTO albums
(
    album_title,
    date_released,
    artist_id
)
VALUES
(
    "A star is born",
    "2008-1-1",
    4
),
(
    "Born this way",
    "2012-1-1",
    4
);
--A star is born
INSERT INTO songs
(
    song_name,
    length,
    genre,
    album_id
)
VALUES
(
    "Shallow",
    250,
    "Country, Rock, Folk Rock",
    10
),
(
    "Always Remember Us this way",
    310,
    "Country",
    10
);
--Born this way
INSERT INTO songs
(
    song_name,
    length,
    genre,
    album_id
)
VALUES
(
    "Bad Romance",
    320,
    "Electro, Pop",
    11
),
(
    "Paparazzi",
    258,
    "Pop",
    11
);


-- 1 song per albums for justine Bieber [5]
INSERT INTO albums
(
    album_title,
    date_released,
    artist_id
)
VALUES
(
    "Purpose",
    "2015-1-1",
    5
),
(
    "Believe",
    "2012-1-1",
    5
);
--Purpose
INSERT INTO songs
(
    song_name,
    length,
    genre,
    album_id
)
VALUES
(
    "Sorry",
    242,
    "Danchall-poptropical",
    12
);
--Believe
INSERT INTO songs
(
    song_name,
    length,
    genre,
    album_id
)
VALUES

(
    "Boyfriend",
    320,
    "Pop",
    13
);


-- 1 song per albums for ariana grande [6]
INSERT INTO albums
(
    album_title,
    date_released,
    artist_id
)
VALUES
(
    "Dangerous Woman",
    "2016-1-1",
    6
),
(
    "Thank U Next",
    "2019-1-1",
    6
);
--Dangerous Woman
INSERT INTO songs
(
    song_name,
    length,
    genre,
    album_id
)
VALUES
(
    "Into You",
    320,
    "EDM House",
    14
);
--Thank U Next
INSERT INTO songs
(
    song_name,
    length,
    genre,
    album_id
)
VALUES

(
    "Thank U Next",
    246,
    "Pop, R&B",
    15
);

-- 1 song per albums for bruno mars [7]
INSERT INTO albums
(
    album_title,
    date_released,
    artist_id
)
VALUES
(
    "24k Magic",
    "2016-1-1",
    7
),
(
    "Earth to mars",
    "2011-1-1",
    7
);
--24K Magic
INSERT INTO songs
(
    song_name,
    length,
    genre,
    album_id
)
VALUES
(
    "24K Magic",
    400,
    "Funk, Disco, R&B",
    16
);
--Earth to mars
INSERT INTO songs
(
    song_name,
    length,
    genre,
    album_id
)
VALUES

(
    "Lost",
    250,
    "Pop",
    17
);

--advanced Selection
SELECT  * FROM songs 
WHERE id = 11; --get 1 data that has id = 11


--exclude from the records
SELECT  * FROM songs 
WHERE id != 11; --get all date except id = 11

--greater than or equal
SELECT  * FROM songs 
WHERE id < 11; --id=11 and greater will not be visible 

SELECT  * FROM songs 
WHERE id <= 11;
SELECT  * FROM songs 
WHERE id > 11; 
SELECT  * FROM songs 
WHERE id >= 11; --gets na yan ahahaha 

--get specific ID using (OR)
SELECT  * FROM songs 
WHERE id = 1 OR id = 3 OR id = 5;
--will display only id=3 and id=5    

--get specific ID using (IN)
SELECT  * FROM songs 
WHERE id IN (1, 3, 5);
--same output in the above 

SELECT * FROM songs
WHERE genre IN ("Pop", "Kpop");

--combining conditions 
SELECT * FROM songs
WHERE  album_id = 4 AND id < 8; 
--both should be true so that it can display

--find partial matches 
SELECT * FROM songs 
WHERE song_name
LIKE "%a"; --select keyword from the end
--"a" to the last
--not case sensitive
--"A%" serach to the start
SELECT * FROM songs 
WHERE song_name
LIKE "ba%";

--if the letter is in the middle 
SELECT * FROM songs 
WHERE song_name
LIKE "%a%"; --the "a" can also be change to word just example that is why it is an example

SELECT * FROM albums 
WHERE date_released
LIKE '%201%';

SELECT * FROM albums 
WHERE date_released
LIKE "20_8-01-01";


--sorting keywords
SELECT * FROM songs
ORDER BY song_name
ASC; --means ascending

select distinct song_name from songs

--table joins 
--combine artist trable and albums table 

SELECT * FROM artists
JOIN albums 
ON artists.id = albums.artist_id;

-- more than 2 tables
SELECT * FROM artists
JOIN albums ON  artists.id = albums.artist_id
JOIN songs ON albums.id = songs.album_id;

-- select columns to be included in a table 
SELECT artists.name, 
       albums.album_title,
       songs.song_name
FROM artists
JOIN albums 
ON artists.id = albums.artist_id
JOIN songs ON albums.id = songs.album_id;

--show artist without record on the right side of the joined table 

SELECT * FROM artists
LEFT JOIN  albums 
ON artists.id = albums.artist_id;
